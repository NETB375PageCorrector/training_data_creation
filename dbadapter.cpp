#include "dbadapter.h"

DBAdapter::DBAdapter()
{
    // database setup
    if (!this->db.isOpen())
        this->db = QSqlDatabase::addDatabase( "QSQLITE" );
    this->db.setDatabaseName( dbName ); // set the database name
    if (this->db.open()) // open the database
        qDebug() << "DB is open";
    else
        qDebug() << "Failed to open DB" << db.lastError();
    qDebug() << db.isDriverAvailable("QSQLITE") << db.isOpen() << db.isValid();
    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.exec( "CREATE TABLE IF NOT EXISTS blobs ( UUID TEXT, Timestamp TEXT, Character CHAR, Bitmap BLOB )" ); // execute the query
}

DBAdapter::DBAdapter(QString dbName)
{
    this->dbName = dbName;
    // database setup
    this->db = QSqlDatabase::addDatabase( "QSQLITE" );
    this->db.setDatabaseName( dbName ); // set the database name
    this->db.open(); // open the database
    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.exec( "CREATE TABLE IF NOT EXISTS blobs ( UUID TEXT, Timestamp TEXT, Character CHAR, Bitmap BLOB )" ); // execute the query
}

DBAdapter::~DBAdapter() {
    qDebug() << "Destructor called";
    QString connection = this->db.connectionName();
    this->db.close(); // close the database
    this->db = QSqlDatabase();
    this->db.removeDatabase(connection);
}

QString DBAdapter::generateUUID() {
    QString result;
    bool isUnique = false; // new flag
    while (!isUnique) { // repeat until a unique UUID is generated
        result = QUuid::createUuid().toString(); // create new UUID and convert to string
        QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
        query.prepare("SELECT blobs.UUID FROM blobs WHERE UUID = :uuid LIMIT 1;"); // prepare a new query
        query.bindValue(":uuid", result); // bind UUID value
        if( !query.exec() ) {
            qDebug() << "Error accessing the database:\n" << query.lastError().text();
            return NULL; // UUID could not be generated because there is no connection to database
        }
        else isUnique = true; // the the flag to true -> current UUID is unique
    }
    return result;
}

void DBAdapter::writeBlob(char character, QPixmap blob) {
    /** Prepare data */
    QByteArray inByteArray; // Buffer storage
    QBuffer inBuffer( &inByteArray ); // Buffer
    inBuffer.open( QIODevice::WriteOnly ); // Open buffer
    blob.save( &inBuffer, "BMP" ); // write inPixmap into inByteArray in BMP format
    inBuffer.close(); // close the buffer
    QString newUUID = this->generateUUID(); // Generate new UUID
    QDateTime UTC(QDateTime::currentDateTime().toUTC()); // Get UTC

    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.prepare( "INSERT INTO blobs (UUID, Timestamp, Character, Bitmap) VALUES (:uuid, :timestamp, :char, :bitmapData)" ); // prepare a new query
    query.bindValue( ":uuid", newUUID ); // bind UUID value
    query.bindValue( ":timestamp", UTC ); // bind Timestamp value
    query.bindValue( ":char", character ); // bind character value
    query.bindValue( ":bitmapData", inByteArray ); // bind bitmapData value
    if( !query.exec() ) {
        qDebug() << "Error inserting bitmap into table:\n" << query.lastError();
    }
}

QPixmap DBAdapter::getBlob(QString uuid) {
    QPixmap result; // data holder for result
    /** Get the bitmap from the database */
    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.prepare("SELECT Bitmap FROM blobs WHERE UUID = :uuid LIMIT 1;"); // prepare a new query
    query.bindValue(":uuid", uuid);
    if( !query.exec() )
        qDebug() << "Error getting bitmap from table:\n" << query.lastError();
    if ( query.first() == false ) result = QPixmap(); // not found -> return a NULL QPixmap
    else {
        QByteArray outByteArray = query.value( 0 ).toByteArray(); // Get byte array
        QPixmap outPixmap = QPixmap(); // create data holder
        outPixmap.loadFromData( outByteArray ); // load data from byte array into data holder
        result = outPixmap; // assign loaded data to result
    }

    return result; // return result
}

QVector<QPixmap> DBAdapter::getBlobs(char character) {
    QVector<QPixmap> result; // to hold the results
    /** Get the bitmaps from database */
    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.prepare("SELECT Bitmap FROM blobs WHERE Character = :char;"); // prepare a new query
    query.bindValue(":char", character); // bind character value
    if( !query.exec() )
        qDebug() << "Error getting bitmaps from table:\n" << query.lastError();
    while (query.next()) {
        QByteArray outByteArray = query.value( 0 ).toByteArray(); // Get byte array
        QPixmap outPixmap = QPixmap(); // create data holder
        outPixmap.loadFromData( outByteArray ); // load data from byte array into data holder
        result.append(outPixmap); // push data holder into results vector
    }
    return result; // return the results
}


QVector<QPixmap> DBAdapter::getBlobs(char character, QDateTime start, QDateTime end) {
    QVector<QPixmap> result; // to hold the results
    /** Get the bitmaps from database */
    QSqlQuery query = QSqlQuery( this->db ); // create a new query to run upon the database
    query.prepare("SELECT Bitmap FROM blobs WHERE Character = :char AND timestamp BETWEEN :start AND :end"); // prepare a new query
    query.bindValue(":char", character); // bind character value
    query.bindValue(":start", start); // bind start value
    query.bindValue(":end", end); // bind end value
    if( !query.exec() )
        qDebug() << "Error getting bitmaps from table:\n" << query.lastError();
    while (query.next()) {
        QByteArray outByteArray = query.value( 0 ).toByteArray(); // Get byte array
        QPixmap outPixmap = QPixmap(); // create data holder
        outPixmap.loadFromData( outByteArray ); // load data from byte array into data holder
        result.append(outPixmap); // push data holder into results vector
    }
    return result; // return the results
}

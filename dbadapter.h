#ifndef DBADAPTER_H
#define DBADAPTER_H
#include <QString>
#include <QVector>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QBuffer>
#include <QUuid>
#include <QDateTime>
#include <QPixmap>
#include <QtSql/QSqlDriver>

// TODO: REMOVE IN PRODUCTION USE
#include <QDebug>

class DBAdapter
{
private:
    QString dbName = QString("blobs.db");
    QSqlDatabase db;
    QString generateUUID();
public:
    DBAdapter();  // Constructor
    DBAdapter(QString dbName);  // Constructor
    ~DBAdapter(); // Destructor
    void writeBlob(char character, QPixmap blob); // Add item to database
    QPixmap getBlob(QString uuid); // get the single blob by uuid
    QVector<QPixmap> getBlobs(char character); // get the array of blobs by character
    QVector<QPixmap> getBlobs(char character, QDateTime start, QDateTime end); // get the array of blobs by character from certain time interval
    // TODO: Functions to search for blobs in certain time interval
};

#endif // DBADAPTER_H

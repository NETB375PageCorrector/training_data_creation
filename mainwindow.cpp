#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    RedrawAll();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_GenerateButton_clicked()
{
    RedrawAll();
}

void MainWindow::DrawCharOnImage(QChar character, int column, int line)
{
    QPainter painter;
    QRect rect = QRect(column * 33 + this->leftPadding - 3,
                       line * 33 + this->topPadding - 3,
                       40, 40);
    QRect boundingRect;
    painter.begin(&image);
    painter.setPen(QPen(QColor(Qt::white)));
    painter.setFont(ui->Font->currentFont());
    QFont font = painter.font();
    font.setPointSize(this->fontSize);
    painter.setFont(font);
    painter.drawText(rect, Qt::AlignCenter | Qt::AlignHCenter, character, &boundingRect);
    painter.end();
}

void MainWindow::on_SaveLocationPicker_clicked()
{
    this->directory = QFileDialog::getExistingDirectory(
                this,
                tr("Select Directory"),
                ""
                );
    ui->SaveLocation->setText(this->directory);
    ui->SaveLocationPicker->setDown(false);
}

void MainWindow::on_SaveButton_clicked()
{
    int minFontSize = ui->FontSize->value() - ui->NumberOfSamples->value();
    int maxFontSize = ui->FontSize->value() + ui->NumberOfSamples->value();
    qDebug() << "Font sizes from" << minFontSize << "to" << maxFontSize;

    float minLeftPadding = ui->LeftPadding->value() - ui->LeftPaddingSamplesWindowWidth->value();
    float maxLeftPadding = ui->LeftPadding->value() + ui->LeftPaddingSamplesWindowWidth->value();
    qDebug() << "Left Padding from" << minLeftPadding << "to" << maxLeftPadding;

    float minTopPadding = ui->TopPadding->value() - ui->TopPaddingSamplesWindowWidth->value();
    float maxTopPadding = ui->TopPadding->value() + ui->TopPaddingSamplesWindowWidth->value();
    qDebug() << "Top Padding from" << minTopPadding << "to" << maxTopPadding;

    for (int fontSize = minFontSize; fontSize <= maxFontSize; fontSize++)
    {
        this->fontSize = fontSize;
        float increaseWindowBy = (ui->FontSize->value() - fontSize) / 2.0f;
        for (float leftPadding = minLeftPadding - increaseWindowBy;
             leftPadding <= maxLeftPadding + increaseWindowBy;
             leftPadding += ui->LeftPaddingSamplesStep->value())
        {
            this->leftPadding = leftPadding;
            for (float topPadding = minTopPadding - increaseWindowBy;
                 topPadding <= maxTopPadding + increaseWindowBy;
                 topPadding += ui->TopPaddingSamplesStep->value())
            {
                this->topPadding = topPadding;
                RedrawAll();
                int lineIndex = 0;
                int columnIndex = 0;
                QImage buffer = QImage(32, 32, QImage::Format_Mono);
                QRect rect;
                QString saveLocation;
                db = new DBAdapter();
                for (int i = 0; i < text.length(); i++)
                {
                    if (text[i] == '\xa')
                    {
                        lineIndex++;
                        columnIndex = 0;
                    }
                    else
                    {
                        rect = QRect(columnIndex * 33, lineIndex * 33, 32, 32);
                        columnIndex++;

                        buffer = image.copy(rect);
                        if (ui->SaveToFilesCheckbox->isChecked())
                        {
                            QString font = ui->Font->currentFont().toString();
                            int firstComma = font.indexOf(',');
                            font.resize(firstComma);
                            qDebug() << font;
                            qDebug() << QString::number(fontSize);
                            int index = 0;
                            char ch = text[i].cell();
                            if (ch >= '0' && ch <= '9')
                                index = 0 + ch - '0';
                            else if (ch >= 'a' && ch <= 'z')
                                index = 10 + ch - 'a';
                            else if (ch >= 'A' && ch <= 'Z')
                                index = 36 + ch - 'A';
                            saveLocation = directory + "/" +
                                    ch + "__" +
                                    font + "__" +
                                    QString::number(fontSize) + "__" +
                                    QString::number(leftPadding) + "__" +
                                    QString::number(topPadding) + ".bmp";
                            if (buffer.save(saveLocation))
                                qDebug() << "Saved" << text[i] << "at" << saveLocation;
                            else
                                qDebug() << "Failed to save" << text[i] << "at" << saveLocation;
                        }
                        if (ui->SaveToDBCheckbox->isChecked())
                        {
                            int index = 0;
                            char ch = text[i].cell();
                            if (ch >= '0' && ch <= '9')
                                index = 0 + ch - '0';
                            else if (ch >= 'a' && ch <= 'z')
                                index = 10 + ch - 'a';
                            else if (ch >= 'A' && ch <= 'Z')
                                index = 36 + ch - 'A';
                            db->writeBlob(index, QPixmap::fromImage(buffer));
                        }
                    }
                }
            }
        }
    }
}

void MainWindow::on_Text_textChanged()
{
    if (ui->AutomaticRefresh->isChecked())
        RedrawAll();
}

void MainWindow::RedrawAll()
{
    int lineCount = 1;
    int maxCharsInLine = 0;
    int charsCountBuffer = 0;
    text = ui->Text->toPlainText();
    for (int i = 0; i < text.length(); i++)
    {
        if (text[i] != '\xa') // exclude newline
        {
            charsCountBuffer++;
        }
        else
        {
            if (charsCountBuffer > maxCharsInLine)
                maxCharsInLine = charsCountBuffer;
            lineCount++;
            charsCountBuffer = 0;
        }
    }
    if (charsCountBuffer > maxCharsInLine)
        maxCharsInLine = charsCountBuffer;

    image = QImage(33 * maxCharsInLine, 33 * lineCount, QImage::Format_Mono);
    image.fill(0);

    int lineIndex = 0;
    int columnIndex = 0;
    for (int i = 0; i < text.length(); i++)
    {
        if (text[i] == '\xa')
        {
            lineIndex++;
            columnIndex = 0;

            QPainter painter;
            painter.begin(&image);
            painter.setPen(QPen(QColor(Qt::white)));
            painter.drawLine(0,
                             -1 + lineIndex * 33,
                             33 * maxCharsInLine,
                             -1 + lineIndex * 33);
            painter.end();
        }
        else
        {
            QPainter painter;
            painter.begin(&image);
            painter.setPen(QPen(QColor(Qt::white)));
            painter.drawLine(32 + columnIndex * 33,
                             0,
                             32 + columnIndex * 33,
                             33 * lineCount);
            painter.end();
            DrawCharOnImage(text[i], columnIndex, lineIndex);
            columnIndex++;
        }
    }

    ui->Image->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_Font_currentFontChanged(const QFont &f)
{
    if (autoRefresh)
        RedrawAll();
}

void MainWindow::on_FontSize_valueChanged(int arg1)
{
    if (autoRefresh)
        RedrawAll();
    fontSize = arg1;
}

void MainWindow::on_AutomaticRefresh_toggled(bool checked)
{
    if (checked)
        RedrawAll();
    autoRefresh = checked;
}

void MainWindow::on_DBLocationPicker_clicked()
{
    this->dbLocation = QFileDialog::getOpenFileName(
                this,
                tr("Select Database"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                tr("Database (*.db)")
                );
    ui->DBLocation->setText(this->dbLocation);
    ui->DBLocationPicker->setDown(false);
}

void MainWindow::on_LeftPadding_valueChanged(double arg1)
{
    leftPadding = arg1;
    if (autoRefresh)
        RedrawAll();
}

void MainWindow::on_TopPadding_valueChanged(double arg1)
{
    topPadding = arg1;
    if (autoRefresh)
        RedrawAll();
}

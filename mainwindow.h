#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QFileDialog>
#include <QDebug>
#include <QChar>
#include <QPainter>
#include <QStandardPaths>
#include <QtGlobal>
#include "dbadapter.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 *
 * Holds all of the logic of the program.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief MainWindow constructor
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    /**
     * @brief Destructor
     * Deletes the ui
     */
    ~MainWindow();

private slots:
    /**
     * @brief on_GenerateButton_clicked
     * calls RedralAll()
     */
    void on_GenerateButton_clicked();

    /**
     * @brief on_SaveLocationPicker_clicked
     * Shows a QFileDialog for the user to select a directory
     */
    void on_SaveLocationPicker_clicked();

    /**
     * @brief on_SaveButton_clicked
     * Saves all characters separately with the different font sizes, left paddings and top paddings
     */
    void on_SaveButton_clicked();

    /**
     * @brief on_Text_textChanged
     * If autoRefresh is on, then refresh
     */
    void on_Text_textChanged();

    /**
     * @brief RedrawAll
     * Draws all characters from the Text field on the screen,
     * using the DrawCharOnImage function.
     * It draws a line of width 1px to separate the characters horizontally
     * and vertically.
     * Each character occupies a square of 32x32 pixels.
     */
    void RedrawAll();

    /**
     * @brief on_Font_currentFontChanged
     * @param f the font
     * If autoRefresh is on, then refresh
     */
    void on_Font_currentFontChanged(const QFont &f);

    /**
     * @brief on_FontSize_valueChanged
     * @param arg1 the font size
     * If autoRefresh is on, then refresh
     * Update fontSize
     */
    void on_FontSize_valueChanged(int arg1);

    /**
     * @brief on_AutomaticRefresh_toggled
     * @param checked whether it is checked or not
     * If autoRefresh is on, then refresh
     * Update autoRefresh
     */
    void on_AutomaticRefresh_toggled(bool checked);

    /**
     * @brief on_DBLocationPicker_clicked
     * Shows a QFileDialog for the user to select a database
     */
    void on_DBLocationPicker_clicked();

    /**
     * @brief on_LeftPadding_valueChanged
     * @param arg1 the left padding value
     * If autoRefresh is on, then refresh
     * Update leftPadding
     */
    void on_LeftPadding_valueChanged(double arg1);

    /**
     * @brief on_TopPadding_valueChanged
     * @param arg1 the top padding
     * If autoRefresh is on, then refresh
     * Update topPadding
     */
    void on_TopPadding_valueChanged(double arg1);

private:
    // holds all ui fields
    Ui::MainWindow *ui;
    // stores the image, where all characters are drawn
    QImage image;
    // the directory, where to save the individual images
    QString directory;
    // the location of the databese to save at
    QString dbLocation;
    // the text, which will be drawn
    QString text;
    // the database
    DBAdapter *db;
    // the font size of the characters
    int fontSize = 20;
    // whether to refresh on every change
    bool autoRefresh = false;
    // the left padding
    float leftPadding = 0;
    // the top padding
    float topPadding = 0;
    // draws a character with the given font, font size, left padding and top padding
    // from the center of a 32x32 pixels square at given column and line indexes
    void DrawCharOnImage(QChar character, int column, int line);
};

#endif // MAINWINDOW_H

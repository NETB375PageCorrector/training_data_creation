#-------------------------------------------------
#
# Project created by QtCreator 2017-05-26T16:15:18
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = trainind_data_creation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dbadapter.cpp

HEADERS  += mainwindow.h \
    dbadapter.h

FORMS    += mainwindow.ui
